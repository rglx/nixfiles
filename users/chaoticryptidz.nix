{ config, ... }: {
  users.users.chaoticryptidz = {
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "disk"
      "video"
      "systemd-journal"
      "plugdev"
      "vfio"
      "input"
      "uinput"
    ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL4L1eBZzYXZNGBucTn/eOFp48el9JPiYt9iXQDpBSg/ chaoticryptidz@owo.monster"
    ];
  };
}
