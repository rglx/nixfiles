{ config, ... }: {
  users.users.root = {
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL4L1eBZzYXZNGBucTn/eOFp48el9JPiYt9iXQDpBSg/ chaoticryptidz@owo.monster"
    ];
  };
}
