{ config, pkgs, ... }: {
  programs.sway.enable = true;
  programs.xwayland.enable = true;
  #services.xserver.enable = true;
  #systemd.services.display-manager.enable = true;
  #services.xserver.displayManager.sddm.enable = true;
}
