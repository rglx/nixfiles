{ config, ... }: {
  services.xserver.libinput.enable = true;
  services.tlp.enable = true;
  powerManagement.enable = true;
  powerManagement.powertop.enable = true;
  hardware.acpilight.enable = true;
}
