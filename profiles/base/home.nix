{ inputs, tree, config, lib, ... }:
with lib; {
  options.home-manager.users = mkOption {
    type = types.attrsOf (types.submoduleWith {
      modules = [ ];
      specialArgs = {
        inherit inputs tree;
        nixos = config;
      };
    });
  };
  config = {
    home-manager = {
      useGlobalPkgs = true;
      useUserPackages = true;
    };
  };
}
