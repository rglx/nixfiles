{ pkgs, ... }: {
  environment.systemPackages = with pkgs;
    [ buildPackages.buildPackages.kitty.terminfo ];
}
