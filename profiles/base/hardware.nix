{ lib, pkgs, ... }: {
  hardware.enableRedistributableFirmware = true;
  hardware.enableAllFirmware = true;
  hardware.cpu.intel.updateMicrocode =
    lib.mkIf ("${pkgs.system}" == "x86_64-linux") true;
  hardware.cpu.amd.updateMicrocode =
    lib.mkIf ("${pkgs.system}" == "x86_64-linux") true;
  hardware.wirelessRegulatoryDatabase = true;
}
