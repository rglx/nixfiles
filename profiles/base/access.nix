{ tree, config, lib, pkgs, ... }: {
  users.defaultUserShell = pkgs.zsh;
  security.sudo.wheelNeedsPassword = lib.mkForce false;
}
