{ inputs, config, lib, ... }: {
  nix = {
    nixPath = [ "nixpkgs=${inputs.nixpkgs}" ];
    extraOptions = lib.optionalString
      (lib.versionAtLeast config.nix.package.version "2.4") ''
        experimental-features = nix-command flakes
      '';
    trustedUsers = [ "root" "@wheel" ];
  };
  nixpkgs = {
    config = { allowUnfree = true; };
    overlays = [ (import ../../overlay) ];
  };
}
