{ ... }: {
  nix.binaryCaches = [ "https://cachix.org/api/v1/cache/serokell" ];

  nix.binaryCachePublicKeys =
    [ "serokell.cachix.org-1:5DscEJD6c1dD1Mc/phTIbs13+iW22AVbx0HqiSb+Lq8=" ];
}
