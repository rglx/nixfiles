{ pkgs, lib, ... }: {
  boot.binfmt = {
    emulatedSystems = [ "aarch64-linux" ];
    registrations.aarch64-linux = {
      interpreter = lib.mkForce "${pkgs.qemu}/bin/qemu-aarch64";
    };
  };
}
