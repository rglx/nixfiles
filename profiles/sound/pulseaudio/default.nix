{ config, pkgs, ... }: {
  sound.enable = true;
  nixpkgs.config.pulseaudio = true;
  hardware.pulseaudio = {
    enable = true;
    #support32Bit = true;
    extraModules = with pkgs; [ pkgs.pulseaudio-modules-bt ];
    package = pkgs.pulseaudioFull;
    extraConfig = "\n      load-module module-switch-on-connect\n    ";
  };
  environment.systemPackages = with pkgs; [ pavucontrol ];
}
