{ config, lib, ... }: {
  networking = {
    networkmanager = {
      enable = true;
      connectionConfig = { "ipv6.ip6-privacy" = lib.mkForce 0; };
    };
  };
  programs.nm-applet.enable = true;
}
