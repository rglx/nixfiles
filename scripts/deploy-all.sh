#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
REPO_ROOT="${SCRIPT_DIR}/.."
cd $REPO_ROOT

git add .

deploy -s ".#lappy"
deploy -s ".#hetzner-vm"
#deploy -s ".#raspberry" -- --no-sandbox
