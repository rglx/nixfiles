#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
REPO_ROOT="${SCRIPT_DIR}/.."
cd $REPO_ROOT

# re-run as root
if [ "$EUID" -ne 0 ]; then
sudo ${BASH_SOURCE[0]} $@
exit
fi

nix flake update