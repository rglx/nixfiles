{ fetchzip }:
fetchzip {
  name = "comic-code-1.0";

  url =
    "https://cdn.discordapp.com/attachments/780878889924165652/919700644347142214/COCOD.ZIP";
  sha256 = "sha256-MzEkMAopjpQBk24nRVDAtGNeF6f61XGww4rya2cU8TU=";

  postFetch = ''
    mkdir -p $out/share/fonts
    unzip -j $downloadedFile \*.otf -d $out/share/fonts/truetype
  '';
}
