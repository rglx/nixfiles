{ stdenv, fetchFromGitLab, lib, zig }:
stdenv.mkDerivation rec {
  pname = "zar";
  version = "latest-1";

  src = fetchFromGitLab {
    owner = "ChaotiCryptidz";
    repo = "zar";
    rev = "5f2d473ef89c0cbf77c2a66a22ba457fe4390dd9";
    sha256 = "sha256-w0qXFJEL+Zbmsl9vUmrnG2P59zVo7RQdi+Fbvb3ucgw=";
    #sha256 = lib.fakeSha256;
    fetchSubmodules = true;
  };

  nativeBuildInputs = [ zig ];

  preBuild = ''
    export HOME=$TMPDIR
  '';

  installPhase = ''
    zig build -Drelease-safe --prefix $out install
  '';
}
