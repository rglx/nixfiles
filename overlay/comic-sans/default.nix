{ lib, fetchzip }:
fetchzip {
  name = "comic-code-1.0";

  url =
    "https://cdn.discordapp.com/attachments/890298617833857055/931224386143408239/comic.zip";
  sha256 = "sha256-QTCdQosgHiRkedGT2PjVzeaOqYiotLYpXNyGe9k9nuk=";

  postFetch = ''
    mkdir -p $out/share/fonts
    unzip -j $downloadedFile \*.ttf -d $out/share/fonts/truetype
  '';
}
