final: prev: {
  comic-sans = final.callPackage ./comic-sans { };
  comic-code = final.callPackage ./comic-code { };
  zar = final.callPackage ./zar { };
  gobar = final.callPackage ./gobar { };
  invidious-latest = final.callPackage ./invidious-latest {
    # needs a specific version of lsquic
    lsquic = final.callPackage ./invidious-latest/lsquic.nix { };
  };
  multimc = prev.multimc.override {
    msaClientID = "499546d9-bbfe-4b9b-a086-eb3d75afb78f";
  };
}
