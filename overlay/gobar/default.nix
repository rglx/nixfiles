{ stdenv, fetchFromGitLab, buildGoModule, lib, go }:
buildGoModule rec {
  pname = "gobar";
  version = "latest-1";

  src = fetchFromGitLab {
    owner = "ChaotiCryptidz";
    repo = "gobar";
    rev = "c7891afcd92bf0a31077098c66cbc4070d8b9340";
    sha256 = "sha256-tuHHB7xX4vqsBwe/LsNg5+v+gzg6xotZ9MDK+fk3mR4=";
    #sha256 = lib.fakeSha256;
  };

  runVend = false;
  vendorSha256 = "sha256-37QYc+gLzCW1jefAQNy4AbabckJ4jO1sDOiUZTsLgWo=";
  #vendorSha256 = lib.fakeSha256;

  preBuild = ''
    export HOME=$TMPDIR
  '';
}
