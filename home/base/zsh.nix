{ config, pkgs, ... }: {
  home.packages = with pkgs; [ exa ripgrep vault ];

  programs.zsh = {
    enable = true;
    enableSyntaxHighlighting = true;
    enableAutosuggestions = true;
    oh-my-zsh = {
      enable = true;
      theme = "afowler";
      plugins = [ ];
    };
    shellAliases = {
      nixdirfmt = "nixfmt $(fd -e nix)";
      exa = "exa --time-style long-iso";
      ls = "exa -G";
      la = "exa -Ga";
      ll = "exa -l";
      lla = "exa -lga";
      sys = "systemctl";
      sysu = "systemctl --user";
      logu = "journalctl --user";
      log = "journalctl";
      dmesg = "dmesg -HP";
      hg = "history 0 | rg";
    };
  };
  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
  };
}
