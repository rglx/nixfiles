{ ... }: {
  programs.ssh.enable = true;
  programs.ssh.matchBlocks = {
    "lappy" = {
      user = "lappy.tailscale-internal.genderfucked.monster";
      hostname = "100.115.10.34";
    };
    "hetzner-vm" = {
      user = "root";
      hostname = "hetzner-vm.tailscale-internal.genderfucked.monster";
    };
    "raspberry" = {
      user = "root";
      hostname = "raspberry.tailscale-internal.genderfucked.monster";
    };
  };
}
