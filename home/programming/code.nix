{ config, pkgs, ... }:

{
  programs.vscode = {
    enable = true;
    userSettings = {
      "window.zoomLevel" = -1;
      "editor.fontSize" = 24;
      "editor.fontFamily" = "'Comic Code'";
      "terminal.integrated.fontSize" = 24;
      "editor.codeLensFontFamily" = "'Comic Code'";
      "editor.inlayHints.fontFamily" = "'Comic Code'";
      "markdown.preview.fontFamily" = "'Comic Code'";
      "terminal.integrated.fontFamily" = "'Comic Code'";
      "files.autoSave" = "afterDelay";
      "synthwave84.disableGlow" = true;
    };
  };
}
