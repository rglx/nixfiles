{ config, pkgs, ... }:

{
  home.packages = with pkgs; [ zig zls ];
  programs.vscode.extensions = with pkgs; [ vscode-extensions.tiehuis.zig ];
}
