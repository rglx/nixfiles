{ config, pkgs, ... }:

{
  home.packages = with pkgs; [ nixfmt ];

  programs.vscode.extensions = with pkgs; [
    vscode-extensions.bbenoist.nix
    vscode-extensions.brettm12345.nixfmt-vscode
  ];

  programs.vscode.userSettings."[nix]" = {
    "editor.defaultFormatter" = "brettm12345.nixfmt-vscode";
  };
}
