{ config, pkgs, ... }:

{
  home.packages = with pkgs; [ go gopls go-outline goimports ];
  programs.vscode.extensions = with pkgs; [ vscode-extensions.golang.go ];
}
