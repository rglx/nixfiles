{ pkgs, lib, ... }:
let
  getExtension = { id, url, sha256, version, updateUrl }: {
    inherit id;
    crxPath = builtins.fetchurl {
      url = "${url}";
      name = "${id}.crx";
      inherit sha256;
    };
    inherit version;
    inherit updateUrl;
  };
  createChromiumExtensionFor = browserVersion:
    { id, sha256, version }: {
      inherit id;
      crxPath = builtins.fetchurl {
        url =
          "https://clients2.google.com/service/update2/crx?response=redirect&acceptformat=crx2,crx3&prodversion=${browserVersion}&x=id%3D${id}%26installsource%3Dondemand%26uc";
        name = "${id}.crx";
        inherit sha256;
      };
      #updateUrl = "http://clients2.google.com/service/update2/crx?response=updatecheck&x=id%3D${id}%26uc";
      updateUrl = "https://clients2.google.com/service/update2/crx";
      inherit version;
    };
  createChromiumExtension =
    createChromiumExtensionFor (lib.versions.major pkgs.vivaldi.version);
in {
  programs.chromium = {
    enable = true;
    package = pkgs.vivaldi;
    extensions = [
      { id = "cjpalhdlnbpafiamejdnhcphjbkeiagm"; }
      { id = "clngdbkpkpeebahjckkjfobafhncgmne"; }
      { id = "adicoenigffoolephelklheejpcpoolk"; }
    ];
  };
}
