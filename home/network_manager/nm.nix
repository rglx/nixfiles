{ config, ... }: {
  xsession.preferStatusNotifierItems = true;
  services.network-manager-applet.enable = true;
}
