{ pkgs, inputs, ... }: {
  home.packages = with pkgs; [
    jq
    ripgrep
    fd
    pv
    tmux
    socat
    file
    inputs.deploy-rs.defaultPackage.${pkgs.system}
  ];
}
