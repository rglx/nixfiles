{ pkgs, ... }: {
  home.packages = with pkgs; [
    zip
    unzip
    p7zip
    cabextract
    cramfsprogs
    zstd
    zlib
    xz
    gzip
    bzip2
    squashfsTools
    cpio
    lz4
  ];
}
