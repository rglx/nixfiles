{ pkgs, ... }: {
  home.packages = with pkgs; [
    curl
    wget
    dig
    whois
    dnsutils
    rsync
    openssh
    mosh
    nmap
  ];
}
