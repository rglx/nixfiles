{ pkgs, ... }: {
  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    userName = "ChaotiCryptidz";
    userEmail = "chaoticryptidz@owo.monster";
    extraConfig = { credential = { helper = "store"; }; };
  };
}
