{ tree, ... }: {
  # basically everything apart from home.dev.debugging
  imports = with tree; [
    home.dev.all.archives
    home.dev.all.editors
    home.dev.all.extra
    home.dev.all.git
    home.dev.all.info
    home.dev.all.network
    home.dev.all.vcs
  ];
}
