{ pkgs, ... }: {
  home.packages = with pkgs; [
    binwalk
    file
    binutils # strings
    diffoscope
  ];
}
