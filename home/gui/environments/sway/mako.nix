{ config, pkgs, lib, ... }: {
  programs.mako = {
    enable = true;
    defaultTimeout = 3000;
  };
}
