{ ... }: {
  programs.waybar = {
    enable = true;
    settings = [{
      position = "bottom";

      modules-left = [ ];
      modules-center = [ ];
      modules-right = [ "tray" ];
    }];
  };
}
