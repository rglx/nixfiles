{ config, pkgs, lib, tree, ... }: {
  # import default terminal
  imports = with tree; [ home.apps.kitty ];

  home.sessionVariables = {
    XDG_CURRENT_DESKTOP = "sway";
    XDG_SESSION_TYPE = "wayland";
    WLR_DRM_DEVICES = "/dev/dri/card0";
    SDL_VIDEODRIVER = "wayland";
    QT_QPA_PLATFORM = "wayland";
    QT_WAYLAND_DISABLE_WINDOWDECORATION = 1;
    _JAVA_AWT_WM_NONREPARENTING = 1;
    GDK_SCALE = 2;
    GDK_DPI_SCALE = 0.5;
    XCURSOR_SIZE = 64;
    QT_AUTO_SCREEN_SCALE_FACTOR = 1;
  };

  home.packages = with pkgs; [
    grim
    slurp
    wl-clipboard
    jq
    wofi
    wmctrl
    libnotify
    light
    gobar
    libdbusmenu-gtk3
  ];

  wayland.windowManager.sway = {
    enable = true;
    wrapperFeatures.base = true;
    wrapperFeatures.gtk = true;
    config = let
      terminal = "${pkgs.kitty}/bin/kitty";
      menu = "${pkgs.wofi}/bin/wofi -idbt ${pkgs.kitty}/bin/kitty -p '' -W 25%";
      cfg = config.wayland.windowManager.sway.config;
    in {
      bars = [
        {
          position = "top";
          fonts = {
            names = [ "Comic Code" ];
            size = 14.0;
          };
          statusCommand = lib.escapeShellArgs [
            #"/home/chaoticryptidz/Projects/gobar/gobar "
            "${pkgs.gobar}/bin/gobar"
            "-config"
            "cpu\\|mem\\|weather\\(Leighton\\ Buzzard\\)\\|bat\\(BAT0\\)\\|time"
          ];

        }
        { command = "${pkgs.waybar}/bin/waybar"; }
      ];

      input = { "*" = { xkb_layout = "gb"; }; };

      fonts = {
        names = [ "Comic Code" ];
        size = 18.0;
      };

      modifier = "Mod1";
      terminal = "${pkgs.kitty}/bin/kitty";

      startup = [
        # gets blueman applet working for some reason
        {
          command = "dbus-update-activation-environment DISPLAY";
          always = true;
        }
        {
          command = "nm-applet";
          always = true;
        }
      ];

      seat = { "*" = { "xcursor_theme" = "Adwaita 24"; }; };

      gaps = {
        top = 10;
        bottom = 10;
        left = 10;
        right = 10;
        inner = 20;
        outer = 20;
        smartGaps = true;
      };

      window = {
        border = 4;
        titlebar = false;
      };

      keybindings = {
        "${cfg.modifier}+Return" = "exec ${cfg.terminal}";

        "${cfg.modifier}+Left" = "focus left";
        "${cfg.modifier}+Down" = "focus down";
        "${cfg.modifier}+Up" = "focus up";
        "${cfg.modifier}+Right" = "focus right";

        "${cfg.modifier}+Shift+Left" = "move left";
        "${cfg.modifier}+Shift+Down" = "move down";
        "${cfg.modifier}+Shift+Up" = "move up";
        "${cfg.modifier}+Shift+Right" = "move right";

        "${cfg.modifier}+Shift+space" = "floating toggle";
        "${cfg.modifier}+space" = "focus mode_toggle";

        "XF86AudioRaiseVolume" =
          "exec pactl set-sink-volume $(pacmd list-sinks |awk '/* index:/{print $3}') +5%";
        "XF86AudioLowerVolume" =
          "exec pactl set-sink-volume $(pacmd list-sinks |awk '/* index:/{print $3}') -5%";
        "XF86AudioMute" =
          "exec pactl set-sink-mute $(pacmd list-sinks |awk '/* index:/{print $3}') toggle";
        "XF86AudioMicMute" =
          "exec pactl set-source-mute $(pacmd list-sources |awk '/* index:/{print $3}') toggle";
        "XF86MonBrightnessDown" = "exec sudo xbacklight -time 1 -dec +5";
        "XF86MonBrightnessUp" = "exec sudo xbacklight -time 1 -inc +5";
        "Print" =
          "exec ${pkgs.grim}/bin/grim -t png - | ${pkgs.wl-clipboard}/bin/wl-copy -t image/png";
        "Shift+Print" = ''
          exec ${pkgs.grim}/bin/grim -t png -g "$(${pkgs.slurp}/bin/slurp -d)" - | ${pkgs.wl-clipboard}/bin/wl-copy -t image/png'';

        "${cfg.modifier}+d" = "exec ${cfg.menu}";
        "${cfg.modifier}+f" = "fullscreen";

        "${cfg.modifier}+Shift+q" = "kill";
        "${cfg.modifier}+Shift+c" = "reload";

        "${cfg.modifier}+r" = "mode resize";
      } // (lib.foldl lib.recursiveUpdate { } (map (workspace: {
        "${cfg.modifier}+${workspace}" = "workspace ${workspace}";
        "${cfg.modifier}+Shift+${workspace}" =
          "move container to workspace ${workspace}";
      }) [ "1" "2" "3" "4" "5" "6" "7" "8" "9" ]));
    };
  };
}

