{ config, lib, ... }:

{
  services.gammastep = {
    enable = true;
    tray = true;
    latitude = "51.927031";
    longitude = "-0.654746";
  };
}
