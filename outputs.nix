{ self, nixpkgs, home-manager, deploy-rs, ... }@inputs:
let
  mkTree = import ./tree.nix { inherit (nixpkgs) lib; };
  tree = mkTree {
    inherit inputs;
    folder = ./.;
    config = {
      "hosts/*/services".functor.enable = true;
      "hosts/*/home".functor.enable = true;
      "hosts/*/profiles".functor.enable = true;
      "profiles/*".functor.enable = true;
      "profiles/sound/*".functor.enable = true;
      "profiles/connectivity/*".functor.enable = true;
      "profiles/gaming/*".functor.enable = true;
      "profiles/gui/environments/*".functor.enable = true;
      "users/*".functor.enable = true;
      "home/*".functor.enable = true;
      "home/gui/environments/*".functor.enable = true;
      "home/apps/*".functor.enable = true;
      "home/gaming/emulators/*".functor.enable = true;
      "home/gaming/games/*".functor.enable = true;
      "home/gaming/platforms/*".functor.enable = true;

      "home/programming/languages/*".functor.enable = true;
      "modules/nixos" = {
        functor = {
          enable = true;
          external = [ ];
        };
      };
      "modules/home" = {
        functor = {
          enable = true;
          external = [ ];
        };
      };
    };
  };
in {
  #legacyPackages.tree = tree;
  nixosConfigurations = import ./hosts ({ inherit tree; } // inputs);

  deploy.nodes.lappy = {
    hostname = "lappy.tailscale-internal.genderfucked.monster";
    profiles.system = {
      user = "root";
      path = deploy-rs.lib.x86_64-linux.activate.nixos
        self.nixosConfigurations.lappy;
    };
  };
  deploy.nodes.hetzner-vm = {
    hostname = "hetzner-vm.tailscale-internal.genderfucked.monster";
    username = "root";
    profiles.system = {
      user = "root";
      path = deploy-rs.lib.x86_64-linux.activate.nixos
        self.nixosConfigurations.hetzner-vm;
    };
  };
  deploy.nodes.raspberry = {
    hostname = "raspberry.tailscale-internal.genderfucked.monster";
    username = "root";
    profiles.system = {
      user = "root";
      path = deploy-rs.lib.aarch64-linux.activate.nixos
        self.nixosConfigurations.raspberry;
    };
  };
}
