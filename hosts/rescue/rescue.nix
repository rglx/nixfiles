{ tree, modulesPath, config, pkgs, lib, ... }:

{
  imports = with tree; [
    users.root
    users.chaoticryptidz
    profiles.base
    profiles.gui
    profiles.gui.environments.sway
    profiles.connectivity.network_manager
    profiles.connectivity.ios
    profiles.tor
    (modulesPath + "/installer/cd-dvd/installation-cd-base.nix")
  ];

  home-manager.users.root = {
    imports = with tree; [ home.base home.dev.all ];
  };
  home-manager.users.chaoticryptidz = {
    imports = with tree; [
      home.base
      home.gui
      home.gui.environments.sway
      home.dev.all
      home.network_manager
      home.apps.vivaldi
      home.programming
      home.programming.languages.nix
    ];
  };

  users.users.root.initialPassword = "";
  users.users.chaoticryptidz.initialPassword = "";

  # let vscode, vivaldi, etc work.
  security.unprivilegedUsernsClone = true;

  networking = {
    hostName = "rescue";
    networkmanager.enable = true;
    wireless.enable = lib.mkForce false;
  };

  time.timeZone = "Europe/London";

  system.stateVersion = "21.11";
}

