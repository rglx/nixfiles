{ pkgs, ... }:  let
  passwordFile = "/secrets/mpd-music-sync-password";
in {
  systemd.tmpfiles.rules = [ "d /music 0755 mpd users -" ];
  systemd.services.mpd-music-sync = {
    startAt = "daily";
    requires = [ "network.target" ];
    after = [ "network.target" ];
    path = [ pkgs.rclone ];
    script = ''
      set -x 
      export PASSWORD=$(cat ${passwordFile})
      rclone sync :ftp:/storage/music /music \
        --ftp-host=hetzner-vm.servers.owo.monster \
        --ftp-port=4220 \
        --ftp-user=ftp-user \
        --ftp-pass=$(rclone obscure $PASSWORD)
        chown mpd:users -R /music
    '';
  };
}
