{ pkgs, tree, ... }: {
  imports = [ ./mpd-music-sync.nix ];

  hardware.pulseaudio = {
    extraConfig =
      "load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1";
  };

  environment.systemPackages = with pkgs; [ mpc_cli ];

  systemd.tmpfiles.rules = [ 
    "d /var/lib/mpd 0755 mpd mpd -"
    "d /var/lib/mpd/data 0755 mpd mpd -"
    "d /var/lib/mpd/playlists 0755 mpd mpd -" ];

  services.mpd = {
    enable = true;
    dataDir = "/var/lib/mpd/data";
    playlistDirectory = "/var/lib/mpd/playlists";
    musicDirectory = "/music";
    extraConfig = ''
      host_permissions "127.0.0.1 read,add,control,admin"
      audio_output {
        type "pulse"
        name "Pulseaudio"
        server "127.0.0.1"
      }
    '';
  };

  systemd.services.mpd.serviceConfig.StateDirectory = [ "/music" "/var/lib/mpd" ];
}
