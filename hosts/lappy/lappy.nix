{ tree, config, pkgs, lib, ... }:

{
  imports = with tree; [
    users.root
    users.chaoticryptidz
    profiles.tailscale
    profiles.dnscrypt
    profiles.printing
    profiles.sshd

    # required for dualsense controller
    profiles.kernels.latest

    profiles.laptop

    #profiles.connectivity.bluetooth
    profiles.connectivity.network_manager
    profiles.connectivity.ios

    profiles.sound.pulseaudio

    profiles.gui
    profiles.gui.environments.sway

    profiles.gaming.steam

    # for sci-hub and whenever websites break
    profiles.tor

    # Music! 
    hosts.lappy.profiles.mpd

    # For cross compiling and deploying to raspberry
    profiles.cross.arm64
  ];

  home-manager.users.root = { imports = with tree; [ home.base ]; };
  home-manager.users.chaoticryptidz = {
    imports = with tree; [
      home.base
      home.dev.all
      home.reversing

      home.gui
      home.gui.environments.sway

      home.gaming.emulators.ds
      home.gaming.games.minecraft
      #home.gaming.games.osu
      home.gaming.platforms.steam

      #home.bluetooth
      home.network_manager

      home.apps.vivaldi
      home.apps.telegram
      home.apps.quassel
      home.apps.chromium

      home.programming
      #home.programming.languages.go
      home.programming.languages.nix
    ];
  };

  services.getty.extraArgs =
    [ "--skip-login" "--login-options" "chaoticryptidz" ];

  networking.firewall.enable = true;
  # let vscode, vivaldi, etc work.
  security.unprivilegedUsernsClone = true;

  virtualisation.virtualbox.host.enable = true;
  #virtualisation.virtualbox.host.enableExtensionPack = true;
  users.extraGroups.vboxusers.members = [ "chaoticryptidz" ];

  networking.hostName = "lappy";
  time.timeZone = "Europe/London";
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";

  services.fstrim.enable = true;

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    initrd.availableKernelModules =
      [ "xhci_pci" "ahci" "nvme" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];
    kernelModules = [ "kvm-intel" ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/nixos";
      fsType = "ext4";
    };
    "/boot" = {
      device = "/dev/disk/by-label/nixboot";
      fsType = "vfat";
    };
  };

  system.stateVersion = "21.11";
}

