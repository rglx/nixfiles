{ tree, modulesPath, config, pkgs, lib, ... }:

{
  imports = with tree; [
    users.root
    users.chaoticryptidz

    profiles.tailscale
    profiles.sshd

    #hosts.raspberry.services.router

    (modulesPath + "/installer/sd-card/sd-image-aarch64.nix")
  ];

  home-manager.users.root = {
    imports = with tree; [ home.base home.dev.small ];
  };
  home-manager.users.chaoticryptidz = {
    imports = with tree; [ home.base home.dev.small ];
  };

  boot.kernelPackages = pkgs.linuxPackages_latest;
  networking.hostName = "raspberry";
  time.timeZone = "Europe/London";

  networking.useDHCP = true;
  networking.wireless = {
    enable = false;
    environmentFile = "/secrets/wifi-env";
    networks.BT-JGA898.psk = "@PSK_HOME@";
  };

  sdImage.compressImage = lib.mkForce false;

  system.stateVersion = "21.11";
}

