{ lib, tree, ... }:
let
  externalInterface = "eth0";
  wifiInterface = "wlan0";
  ssid = "Test Wifi";
  password = "UwUPassUwU";
in {
  imports = with tree; [ profiles.dnscrypt ];

  services.dnscrypt-proxy2.settings."listen_addresses" =
    [ "0.0.0.0:53" "[::]:53" ];

  services.hostapd = {
    enable = true;
    interface = wifiInterface;
    ssid = ssid;
    wpaPassphrase = password;
  };

  networking.interfaces = {
    wlan0 = {
      ipAddress = "192.168.2.1";
      prefixLength = 24;
    };
  };

  networking.firewall = {
    trustedInterfaces = [ wifiInterface ];
    checkReversePath = false;
    allowedTCPPorts = [ 53 ];
  };

  networking.nat = {
    enable = true;
    internalIPs = [ "192.168.2.0/24" ];
    externalInterface = externalInterface;
  };

  services.dnsmasq = {
    enable = true;
    servers = [ "192.168.2.1" ];
    extraConfig = ''
      domain=lan
      interface=wlan0
      bind-interfaces
      dhcp-range=192.168.2.10,192.168.2.254,24h
    '';
  };
}
