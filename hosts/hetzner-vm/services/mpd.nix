{ pkgs, ... }: {
  services.mpd = {
    enable = true;
    network.listenAddress = "0.0.0.0";
    dataDir = "/mpd";
    musicDirectory = "/storage/music";
    playlistDirectory = "/mpd/playlists";
    credentials = [{
      passwordFile = "/secrets/mpd-password";
      permissions = [ "read" "add" "control" "admin" ];
    }];
    extraConfig = ''
      host_permissions "127.0.0.1 read,add,control,admin"
      samplerate_converter "1"
      audio_output_format "44100:16:2"
      audio_output {
        type		"httpd"
        name		"HTTP Music Out"
        encoder		"opus"
        port		"8012"
        bitrate		"128000"
        format		"48000:16:2"
        always_on       "yes"
        tags            "yes"
      }
    '';
  };

  systemd.services.mpd.serviceConfig.StateDirectory = [ "/mpd" ];

  services.nginx.virtualHosts."stream.owo.monster" = {
    forceSSL = true;
    enableACME = true;
    locations = {
      "/" = {
        proxyPass = "http://127.0.0.1:8012";
        extraConfig = ''
          auth_basic "Music Password";
          auth_basic_user_file /secrets/music-stream-passwd; 
        '';
      };
    };
  };

  networking.firewall.allowedTCPPorts = [ 6600 ];
}
