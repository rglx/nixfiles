{ ... }:
let
  backupUser = "root";
  backupPaths = [ "/var/lib/postgresql" "/var/lib/vault" "/var/lib/acme" ];
  timerConfig = {
    OnBootSec = "1m";
    OnCalendar = "daily";
  };
in {
  services.restic.backups.hetzner-vm = {
    user = backupUser;
    paths = backupPaths;
    timerConfig = timerConfig;
    repository = "b2:Backups-HetznerVM:/";
    passwordFile = "/secrets/restic-password";
    environmentFile = "/secrets/restic-env";
  };
  services.restic.backups.cassie-hetzner-vm = {
    user = backupUser;
    paths = backupPaths;
    timerConfig = timerConfig;
    repository = "b2:Cryptidz-Backup:HetznerVM";
    passwordFile = "/secrets/restic-password-cassie";
    environmentFile = "/secrets/restic-env-cassie";
  };
}
