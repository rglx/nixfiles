{ pkgs, ... }: {
  services.vault = {
    enable = true;
    package = pkgs.vault-bin;
    address = "127.0.0.1:8200";
    storageBackend = "file";
    extraConfig = ''
      ui = true
    '';
  };
  services.nginx.virtualHosts."vault.owo.monster" = {
    forceSSL = true;
    enableACME = true;
    locations = { "/" = { proxyPass = "http://127.0.0.1:8200"; }; };
  };
  #networking.firewall.allowedTCPPorts = [ 8200 ];
}
