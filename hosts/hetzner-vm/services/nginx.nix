{ ... }: {
  security.acme = {
    defaults = { email = "chaoticryptidz@owo.monster"; };
    acceptTerms = true;
  };
  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    commonHttpConfig = "";
    clientMaxBodySize = "512m";
    serverNamesHashBucketSize = 1024;
  };
  networking.firewall.allowedTCPPorts = [ 80 443 ];
}
