{ ... }: {
  services.quassel = {
    enable = true;
    interfaces = [ "0.0.0.0" ];
  };

  # needed so quassel can access SSL certs
  # TODO: set up SSL
  users.groups.acme.members = [ "quassel" ];

  # Not gonna serve anything there but shrug.
  services.nginx.virtualHosts."quassel.owo.monster" = {
    forceSSL = true;
    enableACME = true;
  };

  services.postgresql.ensureDatabases = [ "quassel" ];
  services.postgresql.ensureUsers = [{
    name = "quassel";
    ensurePermissions."DATABASE quassel" = "ALL PRIVILEGES";
  }];
  services.postgresql.authentication = "host quassel quassel localhost trust";
  networking.firewall.allowedTCPPorts = [ 4242 ];
}
