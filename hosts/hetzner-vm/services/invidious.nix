{ ... }: {
  services.invidious = {
    enable = true;
    port = 3000;
    settings = {
      full_refresh = true;
      https_only = true;
      popular_enabled = false;
      statistics_enabled = true;
      registration_enabled = true;
      channel_threads = 2;
      feed_threads = 2;
      default_user_preferences = {
        locale = "en-US";
        region = "GB";
        captions = [ "English (auto-generated)" "English" "" ];
        dark_mode = true;
        feed_menu = [ "Subscriptions" "Popular" ];
        default_home = "Subscriptions";
        max_results = 30;
        annotations = true;
        annotations_subscribed = true;
        comments = [ ];
        player_style = "invidious";
        related_videos = true;
        autoplay = true;
        continue = false;
        continue_autoplay = true;
        quality = "hd720";
        local = false;
      };
    };
  };

  systemd.services.invidious.serviceConfig = {
    Restart = "always";
    RuntimeMaxSec = "1200";
  };

  services.nginx.virtualHosts."invidious-unproxied.owo.monster" = {
    forceSSL = true;
    enableACME = true;
    locations = { "/".proxyPass = "http://127.0.0.1:3000"; };
  };

  #networking.firewall.allowedTCPPorts = [ 3000 ];
}
