{ ... }: { 
  services.vsftpd = { 
    enable = true;
    extraConfig = ''
      listen_port=4220
      pasv_enable=Yes
      pasv_min_port=51000
      pasv_max_port=51999
    ''; 
    localUsers = true;
    userlistEnable = true;
    userlist = ["ftp-user"];
    localRoot = "/storage";
  };
  users.users.ftp-user = {
    isSystemUser = true;
    group = "ftp-user";
  };
  users.groups.ftp-user = {};
  networking.firewall.allowedTCPPortRanges = [ { from = 51000; to = 51999; } ];
  networking.firewall.allowedTCPPorts = [ 4220 ];
}
