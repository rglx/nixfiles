{ modulesPath, tree, config, pkgs, lib, ... }:

{
  imports = with tree; [
    users.root
    users.chaoticryptidz

    profiles.base
    profiles.tailscale
    profiles.sshd

    hosts.hetzner-vm.services.restic
    hosts.hetzner-vm.services.nginx
    hosts.hetzner-vm.services.invidious
    hosts.hetzner-vm.services.vault
    hosts.hetzner-vm.services.netdata
    hosts.hetzner-vm.services.quassel
    hosts.hetzner-vm.services.mpd
    hosts.hetzner-vm.services.storage-sftp

    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  home-manager.users.root = {
    imports = with tree; [ home.base home.dev.small ];
  };
  home-manager.users.chaoticryptidz = {
    imports = with tree; [ home.base home.dev.small ];
  };

  networking.hostName = "hetzner-vm";
  time.timeZone = "Europe/London";

  networking.firewall.enable = true;
  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [ 22 ];

  networking.enableIPv6 = true;
  networking.usePredictableInterfaceNames = false;
  networking.dhcpcd.enable = true;
  systemd.network = {
    enable = true;
    networks.eth0 = {
      name = "eth0";
      address = [ "2a01:4f9:c010:8beb::/64" ];
      gateway = [ "fe80::1" ];
    };
  };

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.initrd.kernelModules = [ "nvme" ];
  fileSystems."/" = {
    device = "/dev/sda1";
    fsType = "ext4";
  };

  system.stateVersion = "21.11";
}

