{ self, home-manager, nixpkgs, tree, ... }@inputs:
let
  defaultModules = [
    home-manager.nixosModules.home-manager
    tree.impure.modules.nixos
    tree.impure.profiles.base
  ];
  defaultSpecialArgs = {
    tree = tree.impure;
    pureTree = tree.pure;
    inherit inputs;
  };
in {
  lappy = nixpkgs.lib.nixosSystem {
    specialArgs = defaultSpecialArgs;
    system = "x86_64-linux";
    modules = defaultModules ++ [ ./lappy/lappy.nix ];
  };

  rescue-x86_64 = nixpkgs.lib.nixosSystem {
    specialArgs = defaultSpecialArgs;
    system = "x86_64-linux";
    modules = defaultModules ++ [ ./rescue/rescue.nix ];
  };

  hetzner-vm = nixpkgs.lib.nixosSystem {
    specialArgs = defaultSpecialArgs;
    system = "x86_64-linux";
    modules = defaultModules ++ [ ./hetzner-vm/hetzner-vm.nix ];
  };

  # nix --no-sandbox build .#nixosConfigurations.raspberry.config.system.build.sdImage
  raspberry = nixpkgs.lib.nixosSystem {
    specialArgs = defaultSpecialArgs;
    system = "aarch64-linux";
    modules = defaultModules ++ [ ./raspberry/raspberry.nix ];
  };
}
